#!/bin/python

import os
import sys
import subprocess
import xarray as xr
import numpy as np
import re
from kafka import KafkaProducer
import time
import argparse
 

os.environ['TZ'] = 'UTC'
time.tzset

#origdir = '/scratch/pbretonn/media_portal/ingest/prlr_f1h/'
frequency='daily'
dataset='era5'
#var='prlr'

parser = argparse.ArgumentParser()
parser.add_argument("--origdir", type=str)
parser.add_argument("--var", type=str)
parser.add_argument("--startyear", type=int)
parser.add_argument("--startmonth", type=int)
parser.add_argument("--endyear", type=int)
parser.add_argument("--endmonth", type=int)
parser.add_argument("--par", type=int)
args = parser.parse_args()
origdir=args.origdir
var=args.var
startyear=args.startyear
startmonth=args.startmonth
endyear=args.endyear
endmonth=args.endmonth
par=args.par
print("var",var)
print("origdir",origdir)
#i=1
#startyear=1978+i-1
#startyear=1999

#for year in range(startyear,2020,2):
#startyear=2005-i+6
#for year in range(startyear,2021,4):
for year in range(startyear,startyear+1):
  print("year",year)
  for month in range(startmonth,endmonth+1):
    print("month",month)
    for origfile in [f for f in sorted(os.listdir(origdir)) if var in f and f.startswith(str(var)+"_"+str(year)+str(month).zfill(2)) and f.endswith('.nc')]:
      print(origfile)

      ds = xr.open_dataset(os.path.join(origdir,origfile),engine='netcdf4')
#ds = xr.open_dataset("/esarchive/scratch/pbretonn/media_portal/ingest/data/daily/tas/tas_200001.nc",engine='netcdf4')

      my_var=ds[var].values.reshape(ds[var][:,0,0].values.size,ds[var][0].values.size)
#   longitudes=np.tile(ds["longitude"].values,(ds["latitude"].values.size,ds["time"].values.size)).reshape(ds[var].values.size)
#   lat=[]
#   for i in range(ds["latitude"].values.size):
#     lat.append(([ds["latitude"].values[i]]*ds["longitude"].values.size))

#   latitudes=np.array(np.tile(lat,ds["time"].values.size)).reshape(ds[var].values.size)
#   database_array=np.transpose([longitudes,latitudes,my_var])
#   print(database_array)  


      producer = KafkaProducer(bootstrap_servers='morty.int.bsc.es:9092', key_serializer=str.encode, value_serializer=str.encode)
      slice_size=100
      for time_index in range(0,ds.variables["time"].size):
#        date=str(ds["time"].values[time_index])[0:19].replace("-","_").replace("T","_").replace(":","_")
        date=str(ds["time"].values[time_index])[0:10].replace("-","_").replace("T","_").replace(":","_")
        print("time",time_index,date)
        key='timestamp:'+date+'|varMapID:'+str(var.upper())+'_WORLD'
#        print "key", key
#        print("my_var",my_var)
        for space_index in range(0,ds[var][0].values.size/slice_size):
#            print "space_index",space_index
            indexes = []
            my_var_ = []
            arr_list = []
            indexes = np.array([str(number).zfill(6) for number in range(space_index*slice_size, (space_index+1)*slice_size)], dtype=np.str)
            my_var_ = np.array(10000*my_var[time_index:time_index+1,space_index*slice_size:(space_index+1)*slice_size].reshape(slice_size),dtype=np.int)
#         my_var_ = np.array(my_var[time_index:time_index+1,space_index*slice_size:(space_index+1)*slice_size].reshape(slice_size),dtype=np.int)
            arr_list = [indexes, my_var_]
            value = str(np.apply_along_axis(','.join,0,arr_list)).replace("['","").replace("']","").replace(",",":").replace("' '","|").replace("'\n '","|")
#            if time_index == 28:
#              print("my_var_",my_var_)
#            print("value",value)
#         producer.send(str(var.upper())+'_WORLD_PAR'+str(i), key=key, value=value)
            producer.send('PAR'+str(par), key=key, value=value)
            time.sleep(.02)
#  producer.send('TAS_WORLD', key='timestamp:2000_12_1_0_0_0|varMapID:TAS_WORLD', value='817000:2464491|817001:2464416|817002:2464341|817003:2464266')

            producer.flush()
