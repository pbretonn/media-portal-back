#!/bin/bash

cd /scratch/pbretonn/media_portal/ingest
source .modules

var=tas ; par=1 ; starmonth=08 ; endmonth=08
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=tas ; par=2 ; starmonth=09 ; endmonth=09
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=tas ; par=3 ; starmonth=10 ; endmonth=10
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=tas ; par=4 ; starmonth=11 ; endmonth=11
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=tas ; par=5 ; starmonth=11 ; endmonth=11
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=clim-tas ; par=6 ; starmonth=01 ; endmonth=03
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=clim-tas ; par=7 ; starmonth=04 ; endmonth=06
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=clim-tas ; par=8 ; starmonth=07 ; endmonth=09
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=clim-tas ; par=9 ; starmonth=10 ; endmonth=12
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=2020 --startmonth=$startmonth --endyear=2020 --endmonth=$endmonth --par=${par} &> ${var}_par${par}.txt &

var=snowfall ; par=10 ; starmonth=01 ; endmonth=09; year=1992 
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=$year --startmonth=$startmonth --endyear=$year --endmonth=$endmonth --par=${par} &>> ${var}_par${par}.txt 

var=snowfall ; par=10 ; starmonth=04 ; endmonth=04; year=1993
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=$year --startmonth=$startmonth --endyear=$year --endmonth=$endmonth --par=${par} &>> ${var}_par${par}.txt 

var=snowfall ; par=10 ; starmonth=01 ; endmonth=04 
for year in 1996 1998 1999; do
  python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=$year --startmonth=$startmonth --endyear=$year --endmonth=$endmonth --par=${par} &>> ${var}_par${par}.txt 
done

var=snowfall ; par=10 ; starmonth=12 ; endmonth=12; year=1999
python ingest-daily.py --origdir=/esarchive/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=${var} --startyear=$year --startmonth=$startmonth --endyear=$year --endmonth=$endmonth --par=${par} &>> ${var}_par${par}.txt 



#for par in $( seq 1 7 ); do 
#  python ingest-daily.py --origdir=/scratch/pbretonn/media_portal/ingest/prlr_f1h --var=prlr --startyear=$((1979 + ($par - 1) * 5 )) --startmonth=01 --endyear=$((1979 + $par * 5 - 1 )) --endmonth=12 --par=$par &> prlr_par${par}.txt2 &
#done
#var=snowfall
#for par in $( seq 1 7 ); do 
#  python ingest-daily.py --origdir=/scratch/pbretonn/media_portal/ingest/${var}_f1h --var=$var --startyear=$((1979 + ($par - 1) * 3 )) --startmonth=01 --endyear=$((1979 + $par * 3 - 1 )) --endmonth=12 --par=$par &> ${var}_par${par}.txt6 &
#done

wait
